package main

import "fmt"
import "gitlab.com/GKostaras/go-tutorial-projects/05-write-your-first-library-package/exercise/govers"

func main() {
	fmt.Println("Version: ", govers.GoVersion())
}
