package govers

import "runtime"

func GoVersion() string {
	return runtime.Version()
}
